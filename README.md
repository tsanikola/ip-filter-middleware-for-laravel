# IP Filter middleware for Laravel

A middleware for laravel that detects and blocks bad bots or redirects blacklisted IPs and Countries (supports CIDR).

**Filter options:**

    ON/OFF switch.
    Add/Remove IPs from Whitelist.
    Add/Remove IPs from Blacklist.
    Add/Remove blacklisted countries.
    Block/Redirect blacklisted countries and IPs.
    Cloudflare friendly.

**Installation:**

On an existing Laravel project,

- Go to "database/migrations", add the files inside this folder to your project (at the same path) and run migration.
- Inside the "app/Http/Kernel.php" file, go to "$middleware" array and add "\App\Http\Middleware\CheckIP::class," to this array.
- Add the "app/Http/Middleware/CheckIP.php" to the same path of your project (to the Middleware folder).
- Add the "app/Http/Controllers/IPFilterController.php" to the same path of your project (to the Controllers folder). 
- Inside the file "routes\web.php" there are 2 routes. Copy them inside the same file of your project (the one that defines the routes). Then you can change the paths of the url to fit your project.
- Inside the "resources\views" folder there are 2 blade files. Those are the admin panel in the front-end. You can copy them inside your views folder or change them as you please.
- Finally, in order to access the plugin's setting, navigate to the path you specified in the routes.